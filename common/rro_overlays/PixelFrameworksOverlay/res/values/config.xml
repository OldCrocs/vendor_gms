<?xml version="1.0" encoding="utf-8"?>
<!--
/*
** Copyright 2020, Raphielscape LLC. and Haruka LLC.
**
** Licensed under the Apache License, Version 2.0 (the "License");
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<resources>

    <!-- The package name for the system's app prediction service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.intelligence/.AppPredictionService"
    -->
    <string name="config_defaultAppPredictionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiPredictionService</string>

    <!-- Component name that should be granted Notification Assistant access -->
    <string name="config_defaultAssistantAccessComponent" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.common.notification.service.AiAiNotificationAssistantService</string>

    <!-- The component name for the default system attention service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         See android.attention.AttentionManagerService.
    -->
    <string name="config_defaultAttentionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.attention.service.AiAiAttentionService</string>

    <!-- The package name for the system's augmented autofill service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, augmented autofill wil be
         disabled.
         Example: "com.android.augmentedautofill/.AugmentedAutofillService"
    -->
    <string name="config_defaultAugmentedAutofillService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiAugmentedAutofillService</string>

    <!-- The package name for the system's content capture service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content capture will be
         disabled.
         Example: "com.android.contentcapture/.ContentcaptureService"
    -->
    <string name="config_defaultContentCaptureService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentCaptureService</string>

    <!-- The package name for the system's content suggestions service.
         Provides suggestions for text and image selection regions in snapshots of apps and should
         be able to classify the type of entities in those selections.
         This service must be trusted, as it can be activated without explicit consent of the user.
         If no service with the specified name exists on the device, content suggestions wil be
         disabled.
         Example: "com.android.contentsuggestions/.ContentSuggestionsService"
    -->
    <string name="config_defaultContentSuggestionsService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiContentSuggestionsService</string>

    <!-- Colon separated list of package names that should be granted DND access -->
    <string name="config_defaultDndAccessPackages" translatable="false">com.google.android.GoogleCamera:com.google.android.gms:com.google.intelligence.sense:com.google.android.settings.intelligence:com.google.android.apps.wellbeing:com.google.android.apps.safetyhub:com.google.android.dialer</string>

    <!-- The package name for the system's speech recognition service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.speech/.RecognitionService"
    -->
    <string name="config_defaultMusicRecognitionService" translatable="false">com.google.android.googlequicksearchbox/com.google.android.apps.search.soundsearch.service.SoundSearchService</string>
    <string name="config_defaultOnDeviceSpeechRecognitionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiSpeechRecognitionService</string>

    <!-- The component name for the default system rotation resolver service.
        This service must be trusted, as it can be activated without explicit consent of the user.
        See android.service.rotationresolver.RotationResolverService.
    -->
    <string name="config_defaultRotationResolverService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiRotationResolverService</string>

    <!-- The package name for the system's search ui service.
     This service returns search results when provided with an input string.

     This service must be trusted, as it can be activated without explicit consent of the user.
     If no service with the specified name exists on the device, on device search wil be
     disabled.
     Example: "com.android.intelliegence/.SearchUiService"
    -->
    <string name="config_defaultSearchUiService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiSearchUiService</string>

    <!-- The package name for the system's smartspace service.
     This service returns smartspace results.

     This service must be trusted, as it can be activated without explicit consent of the user.
     If no service with the specified name exists on the device, smartspace will be disabled.
     Example: "com.android.intelligence/.SmartspaceService"
    -->
    <string name="config_defaultSmartspaceService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiSmartspaceService</string>

    <!-- The component name for the system-wide captions manager service.
         This service must be trusted, as the system binds to it and keeps it running.
         Example: "com.android.captions/.SystemCaptionsManagerService"
    -->
    <string name="config_defaultSystemCaptionsManagerService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.captions.SystemCaptionsManagerService</string>

    <!-- The package name for the OEM custom system textclassifier service.
         This service must be trusted, as it can be activated without explicit consent of the user.
         Example: "com.android.textclassifier"
         If this is empty, the default textclassifier service (i.e. config_servicesExtensionPackage)
         will be used.
         See android.view.textclassifier.TextClassificationManager.
    -->
    <string name="config_defaultTextClassifierPackage" translatable="false">com.google.android.as</string>

    <!-- The package name for the system's translation service.
     This service must be trusted, as it can be activated without explicit consent of the user.
     If no service with the specified name exists on the device, translation wil be
     disabled.
     Example: "com.android.translation/.TranslationService"
    -->
    <string name="config_defaultTranslationService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiTranslationService</string>

    <!-- Package name of the on-device intelligent processor for ambient audio.
         Ambient audio is the sound surrounding the device captured by the DSP
         or the microphone. In other words, the device is continuously
         processing audio data in background. -->
    <string name="config_systemAmbientAudioIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for audio. The
         difference of 'ambient audio' and 'audio' is that in 'audio', the
         user intentionally and consciously aware that the device is recording
         or using the microphone.
         -->
    <string name="config_systemAudioIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for notification.
         -->
    <string name="config_systemNotificationIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for text. Examples
        include providing autofill functionality based on incoming text
        messages. -->
    <string name="config_systemTextIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for system UI
         features. Examples include the search functionality or the app
         predictor. -->
    <string name="config_systemUiIntelligence" translatable="false">com.google.android.as</string>

    <!-- Package name of the on-device intelligent processor for visual
         features. Examples include the autorotate feature. -->
    <string name="config_systemVisualIntelligence" translatable="false">com.google.android.as</string>

    <!-- The component name for the default system AmbientContextEvent detection service.
        This service must be trusted, as it can be activated without explicit consent of the user.
        See android.service.ambientcontext.AmbientContextDetectionService.
    -->
    <string name="config_defaultAmbientContextDetectionService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.AiAiAmbientContextDetectionService</string>

    <!-- Component name that accepts ACTION_SEND intents for requesting ambient context consent. -->
    <string name="config_defaultAmbientContextConsentComponent">com.google.android.as/com.google.android.apps.miphone.aiai.ambientcontext.ui.ConsentActivity</string>

    <!-- Intent extra key for the caller's package name while requesting ambient context consent.
     -->
    <string name="config_ambientContextPackageNameExtraKey">PACKAGE_NAME</string>

    <!-- Intent extra key for the event code int array while requesting ambient context consent. -->
    <string name="config_ambientContextEventArrayExtraKey">EVENT_ARRAY</string>

    <!-- The package name for the system's wallpaper effects generation service.
    This service returns wallpaper effects results.
    This service must be trusted, as it can be activated without explicit consent of the user.
    If no service with the specified name exists on the device, wallpaper effects
    generation service will be disabled.
    Example: "com.android.intelligence/.WallpaperEffectsGenerationService"
    -->
    <string name="config_defaultWallpaperEffectsGenerationService" translatable="false">com.google.android.as/com.google.android.apps.miphone.aiai.app.wallpapereffects.AiAiWallpaperEffectsGenerationService</string>

    <!-- Indicates whether the system wide captions service should also support
         call captioning.
    -->
    <bool name="config_systemCaptionsServiceCallsEnabled" translatable="false">true</bool>

</resources>
